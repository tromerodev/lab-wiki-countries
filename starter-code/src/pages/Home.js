import React from "react";

export const Home = () => {
  return (
    <div className="col-7">
      <div className="jumbotron jumbotron-fluid">
        <div className="container">
          <h1 className="display-4">Welcome!<span role="img" aria-label="earth">🌍</span></h1>
          <p className="lead">
            Select any country from the list and see the details.
          </p>
        </div>
      </div>
    </div>
  );
};
